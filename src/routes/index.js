import { Switch, Route } from "react-router-dom";
import Confraternization from "../pages/Confraternization";
import Graduation from "../pages/Graduation";
import Home from "../pages/Home";
import Marriage from "../pages/Marriage";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/confraternization">
        <Confraternization />
      </Route>
      <Route exact path="/marriage">
        <Marriage />
      </Route>
      <Route exact path="/graduation">
        <Graduation />
      </Route>
    </Switch>
  );
};

export default Routes;
