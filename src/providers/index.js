import { BeersListProvider } from "./BeersList";
import { ConfraternizationCartProvider } from "./ConfraternizationCart";
import { GraduationCartProvider } from "./GraduationCart";
import { MarriageCartProvider } from "./MarriageCart";

const Providers = ({ children }) => {
  return (
    <BeersListProvider>
      <ConfraternizationCartProvider>
        <GraduationCartProvider>
          <MarriageCartProvider>{children}</MarriageCartProvider>
        </GraduationCartProvider>
      </ConfraternizationCartProvider>
    </BeersListProvider>
  );
};

export default Providers;
