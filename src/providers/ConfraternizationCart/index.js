import { createContext, useContext, useState } from "react";
import { BeersListContext } from "../BeersList";

export const ConfraternizationCartContext = createContext();

export const ConfraternizationCartProvider = ({ children }) => {
  const [confraternizationCart, setConfraternizationCart] = useState([]);
  const [cartButton] = useState(true);
  const { beersList } = useContext(BeersListContext);

  const addToCart = (item) => {
    const newItem = beersList.find((beer) => beer.name === item);
    setConfraternizationCart([...confraternizationCart, newItem]);
  };

  const removeFromCart = (item) => {
    const newCart = confraternizationCart.filter(
      (itemOnCart) => itemOnCart.name !== item.name
    );
    console.log(newCart);
    setConfraternizationCart(newCart);
  };

  return (
    <ConfraternizationCartContext.Provider
      value={{ confraternizationCart, addToCart, removeFromCart, cartButton }}
    >
      {children}
    </ConfraternizationCartContext.Provider>
  );
};
