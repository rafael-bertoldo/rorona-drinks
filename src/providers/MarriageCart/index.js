import { createContext, useContext, useState } from "react";
import { BeersListContext } from "../BeersList";

export const MarriageCartContext = createContext();

export const MarriageCartProvider = ({ children }) => {
  const [marriageCart, setMarriageCart] = useState([]);
  const [cartButton] = useState(true);
  const { beersList } = useContext(BeersListContext);

  const addToCart = (item) => {
    const newItem = beersList.find((beer) => beer.name === item);
    setMarriageCart([...marriageCart, newItem]);
  };

  const removeFromCart = (item) => {
    const newCart = marriageCart.filter(
      (itemOnCart) => itemOnCart.name !== item.name
    );
    setMarriageCart(newCart);
  };

  return (
    <MarriageCartContext.Provider
      value={{ marriageCart, addToCart, removeFromCart, cartButton }}
    >
      {children}
    </MarriageCartContext.Provider>
  );
};
