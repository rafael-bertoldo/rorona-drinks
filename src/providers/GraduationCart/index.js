import { createContext, useContext, useState } from "react";
import { BeersListContext } from "../BeersList";

export const GraduationCartContext = createContext();

export const GraduationCartProvider = ({ children }) => {
  const [graduationCart, setGraduationCart] = useState([]);
  const [cartButton] = useState(true);
  const { beersList } = useContext(BeersListContext);

  const addToCart = (item) => {
    const newItem = beersList.find((beer) => beer.name === item);
    setGraduationCart([...graduationCart, newItem]);
  };

  const removeFromCart = (item) => {
    const newCart = graduationCart.filter(
      (itemOnCart) => itemOnCart.name !== item.name
    );
    setGraduationCart(newCart);
  };

  return (
    <GraduationCartContext.Provider
      value={{ graduationCart, addToCart, removeFromCart, cartButton }}
    >
      {children}
    </GraduationCartContext.Provider>
  );
};
