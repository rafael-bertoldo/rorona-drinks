import { createContext, useState } from "react";
import api from "../../services/api";

export const BeersListContext = createContext();

export const BeersListProvider = ({ children }) => {
  const [beersList, setBeersList] = useState([]);

  function getBeers() {
    api
      .get("/beers")
      .then((res) => setBeersList(res.data))
      .catch((err) => console.log(err));
  }

  return (
    <BeersListContext.Provider value={{ beersList, getBeers }}>
      {children}
    </BeersListContext.Provider>
  );
};
