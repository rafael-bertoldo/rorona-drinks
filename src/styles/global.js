import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;

    }

    :root {
        --white: #F5F5F5;
        --vanilla: #F1EDE0;
        --light-blue: #4EA4D7;
        --medium-blue: #3158B3;
        --dark-blue: #16225B;
        --beige: #D2C6B6;
        --black:#0C090C;
        --red: #bc0c3a;
        --light-brown: #9C5236;
        --medium-brown: #6D443C;
        --dark-brown: #3E211D;
        --orange: #BB863C;
        --gray: #445774;
        --light-green: #B4D69A;
        --medium-green: #718D64;
        --dark-green: #29361E;
        --logo-background: #2C835B;
        --logo-green: #235114;
        --logo-beige: #E9E9CC;
        --logo-brown: #A48424;

    }

    body {
        background: var(--logo-green);
        color: var(--vanilla);
    }

    body, input, button {
        font-family: 'Bree Serif', serif;
        font-size: 1rem;
        background-color: var(--logo-green)
    }

    h1,h2,h3,h4,h5,h6 {
        font-family: 'Indie Flower', cursive;
        font-weight: 700;
    }

    button {
        cursor: pointer;
    }

    a {
        text-decoration: none;
    }
`;
