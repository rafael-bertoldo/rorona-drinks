import logo from "../../assets/zoro-img.png";
import { Container } from "./styles";
import { useContext } from "react";
import { BeersListContext } from "../../providers/BeersList";
import { useHistory } from "react-router-dom";
import Button from "../Button";

const Menu = () => {
  const history = useHistory();
  const { getBeers } = useContext(BeersListContext);
  const handleNavigation = (path) => {
    getBeers();
    return history.push(path);
  };
  return (
    <Container>
      <img src={logo} alt="logo" />
      <Button onClick={() => handleNavigation("/marriage")}>Casamentos</Button>
      <Button onClick={() => handleNavigation("/graduation")}>
        Formaturas
      </Button>
      <Button onClick={() => handleNavigation("/confraternization")}>
        Confraternizações
      </Button>
    </Container>
  );
};

export default Menu;
