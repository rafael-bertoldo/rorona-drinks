import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100px;
  background-color: var(--medium-green);
  position: fixed;

  img {
    width: 83px;
  }
`;
