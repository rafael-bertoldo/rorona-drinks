import { Container } from "./styles";

const Card = ({ title, description, image, pairing, id }) => {
  return (
    <Container>
      <section className="title">
        <h1>{title}</h1>
      </section>
      <div>
        <h4>Beer Description:</h4>
        <h3>{description}</h3>
        <img src={image} alt="rotule" />
      </div>
    </Container>
  );
};

export default Card;
