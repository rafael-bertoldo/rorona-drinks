import styled from "styled-components";

export const Container = styled.div`
  width: 200px;
  max-width: 225px;
  height: 400px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding: 7px;
  border: 2px solid var(--black);
  border-radius: 10px;
  background-color: var(--beige);
  color: var(--medium-green);

  section {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
    height: 50px;
    border-bottom: 2px solid var(--black);

    h1 {
      color: var(--dark-blue);
      font-size: 15px;
    }

    button {
      width: 17px;
      height: 17px;
      margin-top: 0;
    }
  }
  div {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;

    h4 {
      font-size: 1.2rem;
    }

    h3 {
      font-size: 0.8rem;
    }
  }

  img {
    width: 100px;
    height: 250px;
  }
`;
