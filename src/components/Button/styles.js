import styled from "styled-components";

export const Container = styled.button`
  background: ${(props) => (props.whiteSchema ? "#f5f5f5" : "#718D64")};
  color: ${(props) => (props.whiteSchema ? "#718D64" : "#f5f5f5")};
  height: 45px;
  border-radius: 8px;
  border: 2px solid var(--medium-green);
  font-family: "Bree Serif", serif;
  margin-top: 16px;
  width: 100%;
  transition: 0.5s;
  :hover {
    border: 2px solid var(--medium-green);
    background: var(--white);
    color: var(--dark-green);
    cursor: pointer;
  }
`;
