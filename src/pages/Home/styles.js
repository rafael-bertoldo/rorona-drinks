import styled, { keyframes } from "styled-components";

const appearFromTop = keyframes`
from {
    opacity: 0;
    transform: translateY(-50px);
}

to {
    opacity: 1;
    transform: translateY(0px)
}   
`;

const appear = keyframes`
    from {
        opacity: 0;
    }

    to {
        opacity: 1;
    }
`;
const appearFromBottom = keyframes`
from {
    opacity: 0;
    transform: translatey(50px);
}

to {
    opacity: 1;
    transform: translatey(0px)
}   
`;

export const Container = styled.div`
  width: 80%;
  height: 70vh;
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-direction: column;
  margin: 5rem auto;

  h1 {
    font-size: 3rem;
    animation: ${appearFromTop} 2s;
  }

  img {
    animation: ${appear} 6s;
  }

  div {
    display: flex;
    flex: 1;
    width: 80%;
    animation: ${appearFromBottom} 2s;
  }

  button {
    text-transform: uppercase;
  }

  button + button {
    margin-left: 16px;
  }
`;
