import { Container } from "./styles";
import logo from "../../assets/logo.png";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button";
import { useContext } from "react";
import { BeersListContext } from "../../providers/BeersList";

const Home = () => {
  const history = useHistory();
  const { getBeers } = useContext(BeersListContext);
  const handleNavigation = (path) => {
    getBeers();
    return history.push(path);
  };

  return (
    <Container>
      <h1>Seja Bem Vindo à</h1>
      <img src={logo} alt="logo" />
      <div>
        <Button onClick={() => handleNavigation("/marriage")}>
          Casamentos
        </Button>
        <Button onClick={() => handleNavigation("/graduation")}>
          Formaturas
        </Button>
        <Button onClick={() => handleNavigation("/confraternization")}>
          Confraternizações
        </Button>
      </div>
    </Container>
  );
};

export default Home;
