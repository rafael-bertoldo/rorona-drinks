import { useContext } from "react";
import { BeersListContext } from "../../providers/BeersList";
import Card from "../../components/Card";
import { Container, TitleContainer, ToTop } from "../Confraternization/styles";
import Menu from "../../components/Menu";
import Button from "../../components/Button";
import { FiChevronUp, FiShoppingCart, FiTrash2 } from "react-icons/fi";
import { MarriageCartContext } from "../../providers/MarriageCart";

const Marriage = () => {
  const { beersList } = useContext(BeersListContext);
  const { addToCart, removeFromCart, cartButton, marriageCart } =
    useContext(MarriageCartContext);

  return (
    <>
      <Menu />
      <Container>
        <TitleContainer>
          <h2>Casamentos</h2>
        </TitleContainer>
        <span>
          <FiShoppingCart size={40} />
          <span>{marriageCart.length}</span>
        </span>
        <div>
          <ul>
            {beersList &&
              beersList.map((beer) => (
                <li>
                  <Card
                    title={beer.name}
                    description={beer.tagline}
                    image={beer.image_url}
                    pairing={beer.food_pairing}
                    key={beer.id}
                  />
                  {!cartButton ? (
                    <Button onClick={removeFromCart}>
                      <FiTrash2 size={26} />
                    </Button>
                  ) : (
                    <Button onClick={() => addToCart(beer.name)}>
                      <FiShoppingCart size={26} />
                    </Button>
                  )}
                </li>
              ))}
          </ul>
        </div>
      </Container>
      <Container id="cart">
        <TitleContainer>
          <h2>Seu Carrinho</h2>
        </TitleContainer>
        <div>
          <ul>
            {marriageCart &&
              marriageCart.map((beer) => (
                <li>
                  <Card
                    title={beer.name}
                    description={beer.tagline}
                    image={beer.image_url}
                    pairing={beer.food_pairing}
                    key={beer.id}
                  />
                  {cartButton ? (
                    <Button onClick={() => removeFromCart(beer)}>
                      <FiTrash2 size={26} />
                    </Button>
                  ) : (
                    <Button onClick={addToCart}>
                      <FiShoppingCart size={26} />
                    </Button>
                  )}
                </li>
              ))}
          </ul>
        </div>
        <ToTop href="#">
          <FiChevronUp size={64} />
        </ToTop>
      </Container>
    </>
  );
};

export default Marriage;
