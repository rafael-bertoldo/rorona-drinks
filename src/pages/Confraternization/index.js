import { useContext } from "react";
import { BeersListContext } from "../../providers/BeersList";
import Card from "../../components/Card";
import { Container, ToTop, TitleContainer } from "./styles";
import Menu from "../../components/Menu";
import { FiChevronUp, FiShoppingCart, FiTrash2 } from "react-icons/fi";
import Button from "../../components/Button";
import { ConfraternizationCartContext } from "../../providers/ConfraternizationCart";

const Confraternization = () => {
  const { beersList } = useContext(BeersListContext);
  const { addToCart, removeFromCart, cartButton, confraternizationCart } =
    useContext(ConfraternizationCartContext);

  return (
    <>
      <Menu />
      <Container id="confraternization">
        <TitleContainer>
          <h2>Confraternizações</h2>
        </TitleContainer>
        <span>
          <FiShoppingCart size={40} />
          <span>{confraternizationCart.length}</span>
        </span>
        <div>
          <ul>
            {beersList &&
              beersList.map((beer) => (
                <li>
                  <Card
                    title={beer.name}
                    description={beer.tagline}
                    image={beer.image_url}
                    pairing={beer.food_pairing}
                    key={beer.id}
                  />
                  {!cartButton ? (
                    <Button onClick={removeFromCart}>
                      <FiTrash2 size={26} />
                    </Button>
                  ) : (
                    <Button onClick={() => addToCart(beer.name)}>
                      <FiShoppingCart size={26} />
                    </Button>
                  )}
                </li>
              ))}
          </ul>
        </div>
      </Container>
      <Container id="cart">
        <TitleContainer>
          <h2>Seu Carrinho</h2>
        </TitleContainer>
        <div>
          <ul>
            {confraternizationCart &&
              confraternizationCart.map((beer) => (
                <li>
                  <Card
                    title={beer.name}
                    description={beer.tagline}
                    image={beer.image_url}
                    pairing={beer.food_pairing}
                    key={beer.id}
                  />
                  {cartButton ? (
                    <Button onClick={() => removeFromCart(beer)}>
                      <FiTrash2 size={26} />
                    </Button>
                  ) : (
                    <Button onClick={addToCart}>
                      <FiShoppingCart size={26} />
                    </Button>
                  )}
                </li>
              ))}
          </ul>
        </div>
        <ToTop href="#">
          <FiChevronUp size={64} />
        </ToTop>
      </Container>
    </>
  );
};

export default Confraternization;
