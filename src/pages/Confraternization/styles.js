import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  div {
    ul {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      justify-content: center;
      list-style: none;
    }

    li {
      margin: 3rem;
    }
  }

  a {
    margin: 0 auto;
  }
`;

export const ToTop = styled.a`
  color: black;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;

  h2 {
    margin-top: 150px;
    font-size: 3rem;
    margin-bottom: 2rem;
  }

  button {
    width: 70px;
    position: absolute;
    left: 1200px;
    top: 130px;
  }
`;
